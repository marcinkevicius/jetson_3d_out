/*
 * Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Error.h"
#include "Thread.h"

#include <Argus/Argus.h>
#include <EGLStream/EGLStream.h>
#include <EGLStream/NV/ImageNativeBuffer.h>

#include <nvbuf_utils.h>
#include <NvEglRenderer.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctime>

// encoder branch variables
#include <iostream>
#include <fstream>
#include <NvVideoEncoder.h>
#include "nvmmapi/NvNativeBuffer.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <boost/thread/thread.hpp>

#define RENDER_EGL 0

static uint32_t     ENCODER_PIXFMT = V4L2_PIX_FMT_H265; // V4L2_PIX_FMT_H264;
static const int    MAX_ENCODER_FRAMES = 5;
// the file descriptor representing the connection to the client
int connfd = -1;

using namespace Argus;
using namespace EGLStream;

/* Constants */
static std::string               OUTPUT_FILENAME ("output.h265.mp4");
static const uint32_t            MAX_CAMERA_NUM = 2;
static const uint32_t            DEFAULT_FRAME_COUNT = 500+MAX_ENCODER_FRAMES;
static const uint32_t            DEFAULT_FPS = 30;
static const Size2D<uint32_t>    STREAM_SIZE(1920, 1080);
static const Size2D<uint32_t>    DISPLAY_SIZE(STREAM_SIZE.width()*2, STREAM_SIZE.height());

/* Globals */
UniqueObj<CameraProvider>  g_cameraProvider;
#if RENDER_EGL
NvEglRenderer*             g_renderer = NULL;
#endif
int                        g_frames_encoded=0;
int                        g_frames_requested=0;
uint32_t                   g_stream_num = MAX_CAMERA_NUM;
uint32_t                   g_frame_count = DEFAULT_FRAME_COUNT;
SensorMode*                g_SensorMode;

/* Debug print macros */
#define PRODUCER_PRINT(...) printf("PRODUCER: " __VA_ARGS__)
#define CONSUMER_PRINT(...) printf("CONSUMER: " __VA_ARGS__)
#define CHECK_ERROR(expr) \
    do { \
        if ((expr) < 0) { \
            abort(); \
            ORIGINATE_ERROR(#expr " failed"); \
        } \
    } while (0);

namespace ArgusSamples
{
    
/*
    Helper class to map NvNativeBuffer to Argus::Buffer and vice versa.
    A reference to DmaBuffer will be saved as client data in each Argus::Buffer.
    Also DmaBuffer will keep a reference to corresponding Argus::Buffer.
    This class also extends NvBuffer to act as a share buffer between Argus and V4L2 encoder.
*/
class DmaBuffer : public NvNativeBuffer, public NvBuffer
{
public:
    /* Always use this static method to create DmaBuffer */
    static DmaBuffer* create(const Argus::Size2D<uint32_t>& size,
                             NvBufferColorFormat colorFormat,
                             NvBufferLayout layout = NvBufferLayout_Pitch)
    {
        DmaBuffer* buffer = new DmaBuffer(size);
        if (!buffer)
            return NULL;

        if (NvBufferCreate(&buffer->m_fd, size.width(), size.height(), layout, colorFormat))
        {
            delete buffer;
            return NULL;
        }

        /* save the DMABUF fd in NvBuffer structure */
        buffer->planes[0].fd = buffer->m_fd;
        /* byteused must be non-zero for a valid buffer */
        buffer->planes[0].bytesused = 1;

        return buffer;
    }

    /* Help function to convert Argus Buffer to DmaBuffer */
//    static DmaBuffer* fromArgusBuffer(Buffer *buffer)
//    {
//        IBuffer* iBuffer = interface_cast<IBuffer>(buffer);
//        const DmaBuffer *dmabuf = static_cast<const DmaBuffer*>(iBuffer->getClientData());
//
//        return const_cast<DmaBuffer*>(dmabuf);
//    }

    /* Return DMA buffer handle */
    int getFd() const { return m_fd; }

    /* Get and set reference to Argus buffer */
    void setArgusBuffer(Buffer *buffer) { m_buffer = buffer; }
    Buffer *getArgusBuffer() const { return m_buffer; }

private:
    DmaBuffer(const Argus::Size2D<uint32_t>& size)
        : NvNativeBuffer(size),
          NvBuffer(0, 0),
          m_buffer(NULL)
    {
    }

    Buffer *m_buffer;   /* Reference to Argus::Buffer */
};

/* An utility class to hold all resources of one capture session */
class CaptureHolder : public Destructable
{
public:
    explicit CaptureHolder();
    virtual ~CaptureHolder();

    bool initialize(CameraDevice *device);

    CaptureSession* getSession() const
    {
        return m_captureSession.get();
    }

    OutputStream* getStream() const
    {
        return m_outputStream.get();
    }

    Request* getRequest() const
    {
        return m_request.get();
    }

    virtual void destroy()
    {
        delete this;
    }

private:
    UniqueObj<CaptureSession> m_captureSession;
    UniqueObj<OutputStream> m_outputStream;
    UniqueObj<Request> m_request;
};

CaptureHolder::CaptureHolder()
{
}

CaptureHolder::~CaptureHolder()
{
    /* Destroy the output stream */
    m_outputStream.reset();
}

bool CaptureHolder::initialize(CameraDevice *device)
{
    ICameraProvider *iCameraProvider = interface_cast<ICameraProvider>(g_cameraProvider);
    if (!iCameraProvider)
        ORIGINATE_ERROR("Failed to get ICameraProvider interface");

    /* Create the capture session using the first device and get the core interface */
    m_captureSession.reset(iCameraProvider->createCaptureSession(device));
    ICaptureSession *iCaptureSession = interface_cast<ICaptureSession>(m_captureSession);
    IEventProvider *iEventProvider = interface_cast<IEventProvider>(m_captureSession);
    if (!iCaptureSession || !iEventProvider)
        ORIGINATE_ERROR("Failed to create CaptureSession");

    /* Create the OutputStream */
    UniqueObj<OutputStreamSettings> streamSettings(
        iCaptureSession->createOutputStreamSettings(STREAM_TYPE_EGL));
    IEGLOutputStreamSettings *iEglStreamSettings =
        interface_cast<IEGLOutputStreamSettings>(streamSettings);
    if (!iEglStreamSettings)
        ORIGINATE_ERROR("Failed to create EglOutputStreamSettings");

    iEglStreamSettings->setPixelFormat(PIXEL_FMT_YCbCr_420_888);
#if RENDER_EGL
    iEglStreamSettings->setEGLDisplay(g_renderer->getEGLDisplay());
#endif
    iEglStreamSettings->setResolution(STREAM_SIZE);

    m_outputStream.reset(iCaptureSession->createOutputStream(streamSettings.get()));

    /* Create capture request and enable the output stream */
    m_request.reset(iCaptureSession->createRequest());
    IRequest *iRequest = interface_cast<IRequest>(m_request);
    if (!iRequest)
        ORIGINATE_ERROR("Failed to create Request");
    iRequest->enableOutputStream(m_outputStream.get());

    ISourceSettings *iSourceSettings =
            interface_cast<ISourceSettings>(iRequest->getSourceSettings());
    if (!iSourceSettings)
        ORIGINATE_ERROR("Failed to get ISourceSettings interface");
    iSourceSettings->setFrameDurationRange(Range<uint64_t>(1e9/DEFAULT_FPS));
    iSourceSettings->setSensorMode(g_SensorMode);

    return true;
}


/*
 * Argus Consumer Thread:
 * This is the thread acquires buffers from each stream and composite them to
 * one frame. Finally it renders the composited frame through EGLRenderer.
 */
class ConsumerThread : public Thread
{
public:
    explicit ConsumerThread(std::vector<OutputStream*> &streams) :
#if RENDER_EGL
        m_outputFile(NULL),
        m_compositedFrame(0),
#endif
        m_streams(streams),
        //m_framesRemaining(g_frame_count),
        m_framesRemaining(1000),
        m_VideoEncoder(NULL),
        m_gotError(false)

    {
    }
    virtual ~ConsumerThread();

protected:
    /** @name Thread methods */
    /**@{*/
    virtual bool threadInitialize();
    virtual bool threadExecute();
    virtual bool threadShutdown();
    /**@}*/

    std::vector<OutputStream*> &m_streams;
    uint32_t m_framesRemaining;
    UniqueObj<FrameConsumer> m_consumers[MAX_CAMERA_NUM];
    int m_dmabufs[MAX_CAMERA_NUM];
    NvBufferCompositeParams m_compositeParam;
#if RENDER_EGL    
    int m_compositedFrame;
    std::ofstream *m_outputFile;
#endif
    //
    // encoder branch variables
    //
    NvVideoEncoder *m_VideoEncoder;
    bool createVideoEncoder();
    void abort();
    bool m_gotError;

    static bool encoderCapturePlaneDqCallback(
            struct v4l2_buffer *v4l2_buf,
            NvBuffer *buffer,
            NvBuffer *shared_buffer,
            void *arg);
    
};

ConsumerThread::~ConsumerThread()
{
    if (m_VideoEncoder)
        delete m_VideoEncoder;

#if RENDER_EGL    
    if (m_compositedFrame)
        NvBufferDestroy(m_compositedFrame);
    
    if (m_outputFile)
        delete m_outputFile;
#endif
    for (uint32_t i = 0; i < m_streams.size(); i++)
        if (m_dmabufs[i])
            NvBufferDestroy(m_dmabufs[i]);

}

void ConsumerThread::abort()
{
    m_VideoEncoder->abort();
    m_gotError = true;
}

bool ConsumerThread::encoderCapturePlaneDqCallback(struct v4l2_buffer *v4l2_buf,
                                                   NvBuffer * buffer,
                                                   NvBuffer * shared_buffer,
                                                   void *arg)
{
    ConsumerThread *thiz = (ConsumerThread*)arg;

    if (!v4l2_buf)
    {
        thiz->abort();
        ORIGINATE_ERROR("Failed to dequeue buffer from encoder capture plane");
    }

#if RENDER_EGL
    thiz->m_outputFile->write((char *) buffer->planes[0].data,
                              buffer->planes[0].bytesused);
#endif
    int stat = write(connfd, buffer->planes[0].data, buffer->planes[0].bytesused);
    
    CONSUMER_PRINT("GOT FRAME - writing to socket, status %d", stat);
    

    if (thiz->m_VideoEncoder->capture_plane.qBuffer(*v4l2_buf, NULL) < 0)
    {
        thiz->abort();
        ORIGINATE_ERROR("Failed to enqueue buffer to encoder capture plane");
        return false;
    }

    /* GOT EOS from m_VideoEncoderoder. Stop dqthread */
    if (buffer->planes[0].bytesused == 0)
    {
        CONSUMER_PRINT("Got EOS, exiting...\n");
        return false;
    }

    return true;
}

bool ConsumerThread::threadInitialize()
{
    /* Create Video Encoder */
    if (!createVideoEncoder())
        ORIGINATE_ERROR("Failed to create video m_VideoEncoderoder");
    
    /* Create output file code, shoud be ommited*/
#if RENDER_EGL
    m_outputFile = new std::ofstream(OUTPUT_FILENAME.c_str());
    if (!m_outputFile)
        ORIGINATE_ERROR("Failed to open output file.");
#endif    
    /* Stream on */
    int e = m_VideoEncoder->output_plane.setStreamStatus(true);
    if (e < 0) ORIGINATE_ERROR("Failed to stream on output plane");
    
    e = m_VideoEncoder->capture_plane.setStreamStatus(true);
    if (e < 0) ORIGINATE_ERROR("Failed to stream on capture plane");
    
    /* Set video encoder callback */
    // callback after writing to file
    m_VideoEncoder->capture_plane.setDQThreadCallback(encoderCapturePlaneDqCallback);
    
    /* startDQThread starts a thread internally which calls the
       encoderCapturePlaneDqCallback whenever a buffer is dequeued
       on the plane */
    m_VideoEncoder->capture_plane.startDQThread(this); // this = data pointer of this obj
    
    /* Enqueue all the empty capture plane buffers */
    for (uint32_t i = 0; i < m_VideoEncoder->capture_plane.getNumBuffers(); i++)
    {
        struct v4l2_buffer v4l2_buf;
        struct v4l2_plane planes[MAX_PLANES];

        memset(&v4l2_buf, 0, sizeof(v4l2_buf));
        memset(planes, 0, MAX_PLANES * sizeof(struct v4l2_plane));

        v4l2_buf.index = i;
        v4l2_buf.m.planes = planes;

        CHECK_ERROR(m_VideoEncoder->capture_plane.qBuffer(v4l2_buf, NULL));
    }
    
    NvBufferRect dstCompRect[2];
    NvBufferCreateParams input_params = {0};

    // Initialize destination composite rectangles
    // The window layout is as below
    //           <---        DISPLAY_SIZE           --->
    // +-------------------------------+-----------------------------------+
    // |    <--- STREAM_SIZE --->      |     <--- STREAM_SIZE --->         |
    // |                               |                                   |
    // |                               |                                   |
    // |     Frame 0                   |        Frame1                     |
    // |                               |                                   |
    // |                               |                                   |
    // +-------------------------------+-----------------------------------+

    dstCompRect[0].top  = 0;
    dstCompRect[0].left = 0;
    dstCompRect[0].width = STREAM_SIZE.width();
    dstCompRect[0].height = STREAM_SIZE.height();
    
    dstCompRect[1].top  = 0;
    dstCompRect[1].left = STREAM_SIZE.width();
    dstCompRect[1].width = STREAM_SIZE.width();
    dstCompRect[1].height = STREAM_SIZE.height();

    /* Allocate composited buffer */
    input_params.payloadType = NvBufferPayload_SurfArray;
    input_params.width = DISPLAY_SIZE.width();
    input_params.height = DISPLAY_SIZE.height();
    input_params.layout = NvBufferLayout_Pitch;  // DOTO NvBufferLayout_Pitch - NvBufferLayout_BlockLinear ???
    input_params.colorFormat = NvBufferColorFormat_NV12;
    input_params.nvbuf_tag = NvBufferTag_VIDEO_CONVERT;

#if RENDER_EGL
    NvBufferCreateEx(&m_compositedFrame, &input_params);
    
    if (!m_compositedFrame)
        ORIGINATE_ERROR("Failed to allocate composited buffer");
#endif
    /* Initialize composite parameters */
    memset(&m_compositeParam, 0, sizeof(m_compositeParam));
    m_compositeParam.composite_flag = NVBUFFER_COMPOSITE;
    m_compositeParam.input_buf_count = m_streams.size();
    memcpy(m_compositeParam.dst_comp_rect, dstCompRect, sizeof(NvBufferRect) * m_compositeParam.input_buf_count);
    
    for (uint32_t i = 0; i < 2; i++)
    {
        m_compositeParam.dst_comp_rect_alpha[i] = 1.0f;
        m_compositeParam.src_comp_rect[i].top = 0;
        m_compositeParam.src_comp_rect[i].left = 0;
        m_compositeParam.src_comp_rect[i].width = STREAM_SIZE.width();
        m_compositeParam.src_comp_rect[i].height = STREAM_SIZE.height();
    }

    /* Initialize buffer handles. Buffer will be created by FrameConsumer */
    memset(m_dmabufs, 0, sizeof(m_dmabufs));

    /* Create the FrameConsumer */
    for (uint32_t i = 0; i < m_streams.size(); i++)
    {
        m_consumers[i].reset(FrameConsumer::create(m_streams[i]));
    }

    return true;
    // moves right to threadExecute thread
}

bool ConsumerThread::threadExecute()
{
    
    
    
    IEGLOutputStream *iEglOutputStreams[MAX_CAMERA_NUM];
    IFrameConsumer *iFrameConsumers[MAX_CAMERA_NUM];

    for (uint32_t i = 0; i < m_streams.size(); i++)
    {
        iEglOutputStreams[i] = interface_cast<IEGLOutputStream>(m_streams[i]);
        iFrameConsumers[i] = interface_cast<IFrameConsumer>(m_consumers[i]);
        if (!iFrameConsumers[i])
            ORIGINATE_ERROR("Failed to get IFrameConsumer interface");

        /* Wait until the producer has connected to the stream */
        CONSUMER_PRINT("Waiting until producer is connected...\n");
        if (iEglOutputStreams[i]->waitUntilConnected() != STATUS_OK)
            ORIGINATE_ERROR("Stream failed to connect.");
        CONSUMER_PRINT("Producer has connected; continuing.\n");
    }

    clock_t Start; //= clock();
    
    // creating encoder output planes for buffer input
    struct v4l2_buffer v4l2_buf;
    struct v4l2_plane planes[MAX_PLANES];

    memset(&v4l2_buf, 0, sizeof(v4l2_buf));
    memset(planes, 0, MAX_PLANES * sizeof(struct v4l2_plane));
    v4l2_buf.m.planes = planes;

    
//  int prepend_frames = 0;
    /*fill in buffer from main loop, aka copy paste main loop*/
//    for (int bufferIndex = 0; bufferIndex < MAX_ENCODER_FRAMES; bufferIndex++)
//    {
//        v4l2_buf.index = bufferIndex;
//        Buffer* buffer = stream->acquireBuffer();
//        /* Convert Argus::Buffer to DmaBuffer and queue into v4l2 encoder */
//        DmaBuffer *dmabuf = DmaBuffer::fromArgusBuffer(buffer);
//        CHECK_ERROR(m_VideoEncoder->output_plane.qBuffer(v4l2_buf, dmabuf));
//
//    }
    
//    while (m_framesRemaining-- && !m_gotError)
    while (!m_gotError && g_frames_requested < 10){
        boost::this_thread::sleep(boost::posix_time::microseconds(10));
    }

    while (g_frames_encoded < g_frames_requested && !m_gotError)
    {
        for (uint32_t i = 0; i < m_streams.size(); i++)
        {
            Start= clock();
            // get frame from buffer
            /* Acquire a frame */
            UniqueObj<Frame> frame(iFrameConsumers[i]->acquireFrame());
            CONSUMER_PRINT("acquireFrame MS %ld\n", clock() - Start);
            IFrame *iFrame = interface_cast<IFrame>(frame);
            if (!iFrame)
                break;
            
            CONSUMER_PRINT("acquireFrame %d\n", i);

            /* Get the IImageNativeBuffer extension interface */
            NV::IImageNativeBuffer *iNativeBuffer =
                interface_cast<NV::IImageNativeBuffer>(iFrame->getImage());
            if (!iNativeBuffer)
                ORIGINATE_ERROR("IImageNativeBuffer not supported by Image.");

            /* If we don't already have a buffer, create one from this image.
               Otherwise, just blit to our buffer */
            if (!m_dmabufs[i])
            {
                m_dmabufs[i] = iNativeBuffer->createNvBuffer(iEglOutputStreams[i]->getResolution(),
                                                          NvBufferColorFormat_YUV420,
                                                          NvBufferLayout_BlockLinear);
                if (!m_dmabufs[i])
                    CONSUMER_PRINT("\tFailed to create NvBuffer\n");
            }
            else if (iNativeBuffer->copyToNvBuffer(m_dmabufs[i]) != STATUS_OK)
            {
                ORIGINATE_ERROR("Failed to copy frame to NvBuffer.");
            }
        }

        CONSUMER_PRINT("Render frame %d\n", g_frame_count);
        if (m_streams.size() > 1)
        {
#if RENDER_EGL
            /* Composite multiple input to one frame */
            NvBufferComposite(m_dmabufs, m_compositedFrame, &m_compositeParam);
#endif
            //START add frame buffer to encoder too
            if (g_frames_encoded < MAX_ENCODER_FRAMES){
                v4l2_buf.index = g_frames_encoded;
                DmaBuffer *dmabuf = DmaBuffer::create(DISPLAY_SIZE,NvBufferColorFormat_YUV420,NvBufferLayout_BlockLinear);
                NvBufferComposite(m_dmabufs, dmabuf->getFd(), &m_compositeParam);
                CHECK_ERROR(m_VideoEncoder->output_plane.qBuffer(v4l2_buf, dmabuf));
                g_frames_encoded++;
            } else{
                /* Dequeue from encoder first */
                NvBuffer *share_buffer;
                CHECK_ERROR(m_VideoEncoder->output_plane.dqBuffer(v4l2_buf, NULL, &share_buffer, 10/*retry*/));
                
                DmaBuffer *dmabuf = static_cast<DmaBuffer*>(share_buffer);
                
                //DmaBuffer *dmabuf = DmaBuffer::create(DISPLAY_SIZE,NvBufferColorFormat_YUV420,NvBufferLayout_BlockLinear);
                NvBufferComposite(m_dmabufs, dmabuf->getFd(), &m_compositeParam);
                g_frames_encoded++;
                CHECK_ERROR(m_VideoEncoder->output_plane.qBuffer(v4l2_buf, dmabuf));
            }
            //END add frame buffer to encoder too
#if RENDER_EGL            
            g_renderer->render(m_compositedFrame);
#endif            
        }
#if RENDER_EGL
        else
            g_renderer->render(m_dmabufs[0]);
#endif
    }

     /* Send EOS */
    v4l2_buf.m.planes[0].m.fd = -1;
    v4l2_buf.m.planes[0].bytesused = 0;
    CHECK_ERROR(m_VideoEncoder->output_plane.qBuffer(v4l2_buf, NULL));
    
    m_VideoEncoder->capture_plane.waitForDQThread(2000);
    
    CONSUMER_PRINT("Done.\n");

    requestShutdown();

    return true;
}

bool ConsumerThread::threadShutdown()
{
    return true;
}

bool ConsumerThread::createVideoEncoder()
{
    int ret = 0;

    m_VideoEncoder = NvVideoEncoder::createVideoEncoder("enc0");
    if (!m_VideoEncoder)
        ORIGINATE_ERROR("Could not create m_VideoEncoderoder");

    ret = m_VideoEncoder->setCapturePlaneFormat(ENCODER_PIXFMT, DISPLAY_SIZE.width(),DISPLAY_SIZE.height(), 8 * 1024 * 1024);  // was 2 * 1024 * 1024
    if (ret < 0)
        ORIGINATE_ERROR("Could not set capture plane format");

    ret = m_VideoEncoder->setOutputPlaneFormat(V4L2_PIX_FMT_YUV420M, DISPLAY_SIZE.width(),DISPLAY_SIZE.height());
    if (ret < 0)
        ORIGINATE_ERROR("Could not set output plane format");

    ret = m_VideoEncoder->setBitrate(16 * 1024 * 1024); // was 4 * 1024 * 1024
    if (ret < 0)
        ORIGINATE_ERROR("Could not set bitrate");

    ret = m_VideoEncoder->setProfile(V4L2_MPEG_VIDEO_H265_PROFILE_MAIN10);    
    if (ret < 0)
        ORIGINATE_ERROR("Could not set m_VideoEncoderoder profile");

    ret = m_VideoEncoder->setRateControlMode(V4L2_MPEG_VIDEO_BITRATE_MODE_CBR);
    if (ret < 0)
        ORIGINATE_ERROR("Could not set rate control mode");

    ret = m_VideoEncoder->setIFrameInterval(30);
    if (ret < 0)
        ORIGINATE_ERROR("Could not set I-frame interval");

    ret = m_VideoEncoder->setFrameRate(30, 1);
    if (ret < 0)
        ORIGINATE_ERROR("Could not set m_VideoEncoderoder framerate");

//    ret = m_VideoEncoder->setHWPresetType(V4L2_ENC_HW_PRESET_ULTRAFAST);
    ret = m_VideoEncoder->setHWPresetType(V4L2_ENC_HW_PRESET_SLOW);

    if (ret < 0)
        ORIGINATE_ERROR("Could not set m_VideoEncoderoder HW Preset");

    /* Query, Export and Map the output plane buffers so that we can read raw data into the buffers */
    // V4L2_MEMORY_DMABUF from camera buffer
    ret = m_VideoEncoder->output_plane.setupPlane(V4L2_MEMORY_DMABUF, 10, true, false);
    if (ret < 0)
        ORIGINATE_ERROR("Could not setup output plane");

    /* Query, Export and Map the output plane buffers so that we can write m_VideoEncoder data from the buffers */
    ret = m_VideoEncoder->capture_plane.setupPlane(V4L2_MEMORY_MMAP, 6, true, false);
    if (ret < 0)
        ORIGINATE_ERROR("Could not setup capture plane");

    printf("create video encoder return true\n");
    return true;
}


/*
 Set up connection to server before transmitting raw video stream
 */
static bool connect_tcp(char *argv[]){
// the structures representing the server address
    sockaddr_in serverAddr;

    // get the port number
    int port = atoi(argv[2]);
    
    // make sure that the port is within a valid range
    if(port < 0 || port > 65535) ORIGINATE_ERROR("Invalid port numbern");

    // stores the size of the client's address
    socklen_t servLen = sizeof(serverAddr);
    
    // connect to the server
    if((connfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) ORIGINATE_ERROR("Coul not create socket");

    // set the structure to all zeros
    memset(&serverAddr, 0, sizeof(serverAddr));

    // set the server family
    serverAddr.sin_family = AF_INET;

    // convert the port number to network representation
    serverAddr.sin_port = htons(port);

    //  convert the IP from the presentation format (i.e. string)
    //  to the format in the serverAddr structure.
    if(!inet_pton(AF_INET, argv[1], &serverAddr.sin_addr))
        ORIGINATE_ERROR("writing ip address to connect");
    
    // connect to the server. This call will return a socket used
    // used for communications between the server and the client.
    if(connect(connfd, (sockaddr*)&serverAddr, sizeof(sockaddr)) < 0)
        ORIGINATE_ERROR("connection error");

    return true;
}

/*
 * Argus Producer Thread:
 * Open the Argus camera driver and detect how many camera devices available.
 * Create one OutputStream for each camera device. Launch consumer thread
 * and then submit FRAME_COUNT capture requests.
 */
static bool execute(char *argv[])
{
    if (!ArgusSamples::connect_tcp(argv))
        ORIGINATE_ERROR("Failed to create Cosket to server.");
    
    /* Initialize EGL renderer */
#if RENDER_EGL
    g_renderer = NvEglRenderer::createEglRenderer("renderer0", DISPLAY_SIZE.width(),DISPLAY_SIZE.height(), 0, 0);
    if (!g_renderer) ORIGINATE_ERROR("Failed to create EGLRenderer.");
#endif
    /* Initialize the Argus camera provider */
    g_cameraProvider = UniqueObj<CameraProvider>(CameraProvider::create());
    ICameraProvider *iCameraProvider = interface_cast<ICameraProvider>(g_cameraProvider);
    if (!iCameraProvider)
        ORIGINATE_ERROR("Failed to get ICameraProvider interface");
    //printf("Argus Version: %s\n", iCameraProvider->getVersion().c_str());

    /* Get the camera devices */
    std::vector<CameraDevice*> cameraDevices;
    iCameraProvider->getCameraDevices(&cameraDevices);
    if (cameraDevices.size() == 0)
        ORIGINATE_ERROR("No cameras available");
    
    // Get sensor mode
    ICameraProperties *iCameraProperties = interface_cast<ICameraProperties>(cameraDevices[0]);
    if(!iCameraProperties) ORIGINATE_ERROR("Failed to create ICameraProperties interface");
    
    std::vector<SensorMode*> modes;
    iCameraProperties->getBasicSensorModes(&modes);

    printf("Available Sensor modes:\n");
    for(size_t i = 0; i < modes.size(); i++)
    {
        ISensorMode *iSensorMode = interface_cast<ISensorMode>(modes[i]);
        if(!iSensorMode) ORIGINATE_ERROR("Failed to create ISensorMode interface");

        Size2D<uint32_t> size = iSensorMode->getResolution();
        Range<uint64_t> framerate = iSensorMode->getFrameDurationRange();
        printf("mode %zu: %u x %u, FR=%.0f\n", i, size.width(), size.height(), 1e9/framerate.min());
    }
    
    //set sensor 4th mode
    g_SensorMode = modes[2];

    //START creating objects to hold all resources of one capture session per object
    UniqueObj<CaptureHolder> captureHolders[MAX_CAMERA_NUM];
    uint32_t streamCount = cameraDevices.size() < MAX_CAMERA_NUM ?
            cameraDevices.size() : MAX_CAMERA_NUM;
    if (streamCount > g_stream_num)
        streamCount = g_stream_num;
    for (uint32_t i = 0; i < streamCount; i++)
    {
        captureHolders[i].reset(new CaptureHolder);
        if (!captureHolders[i].get()->initialize(cameraDevices[i]))
            ORIGINATE_ERROR("Failed to initialize Camera session %d", i);

    }

    std::vector<OutputStream*> streams;
    for (uint32_t i = 0; i < streamCount; i++)
        streams.push_back(captureHolders[i].get()->getStream());
    //END creating objects to hold all resources of one capture session per object
    
    /* Start the rendering thread */  
    /* Start the ENCODING thread */
    // create empty thread object (inherited phtread class)
    ConsumerThread consumerThread(streams);
    
    // overload virtual function 'initialize'
    PROPAGATE_ERROR(consumerThread.initialize());
    // overload virtual function 'waitRunning'
    PROPAGATE_ERROR(consumerThread.waitRunning());

    /* Submit capture requests */
    //for (uint32_t i = 0; i < g_frame_count; i++)
    while (true)// && g_frames_requested < 505)
    {
        if (g_frames_requested < (g_frames_encoded + 20)){
            for (uint32_t j = 0; j < streamCount; j++)
            {
                ICaptureSession *iCaptureSession =
                        interface_cast<ICaptureSession>(captureHolders[j].get()->getSession());
                Request *request = captureHolders[j].get()->getRequest();
                uint32_t frameId = iCaptureSession->capture(request);
                if (frameId == 0)
                    ORIGINATE_ERROR("Failed to submit capture request");
            }
            g_frames_requested++;
        } else {
            // sleep for 10ms, let consummer encode queued frame requests
            CONSUMER_PRINT("waiting for next frame request");
            boost::this_thread::sleep(boost::posix_time::millisec(10));
        }
    }

    /* Wait for idle */
    for (uint32_t i = 0; i < streamCount; i++)
    {
        ICaptureSession *iCaptureSession =
            interface_cast<ICaptureSession>(captureHolders[i].get()->getSession());
        iCaptureSession->waitForIdle();
    }

    /* Destroy the capture resources */
    for (uint32_t i = 0; i < streamCount; i++)
    {
        captureHolders[i].reset();
    }

    /* Wait for the rendering thread to complete */
    PROPAGATE_ERROR(consumerThread.shutdown());

    /* Shut down Argus */
    g_cameraProvider.reset();

#if RENDER_EGL
    /* Cleanup EGL Renderer */
    delete g_renderer;
#endif

    return true;
}

}; /* namespace ArgusSamples */

int main(int argc, char * argv[])
{
    if (argc != 3) ORIGINATE_ERROR("Not enaugh argutems. ip_nr port_nr");
    if (!ArgusSamples::execute(argv)) return EXIT_FAILURE;
    if (connfd) close(connfd);
    return EXIT_SUCCESS;
}
